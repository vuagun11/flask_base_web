from functools import wraps

from flask import session, url_for, redirect


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get('user') is None:
            return redirect('/user/login')
        return f(*args, **kwargs)

    return decorated_function
