import os

if os.environ.get('PRODUCTION'):
    WEB_CONFIG = {
        'CACHE_REDIS_HOST': '127.0.0.1',
        'CACHE_KEY_PREFIX': 'site.com',
        'PERMANENT_SESSION_LIFETIME': ''
    }
else:
    WEB_CONFIG = {
        'CACHE_REDIS_HOST': '127.0.0.1',
        'CACHE_KEY_PREFIX': 'site.com',
        'PERMANENT_SESSION_LIFETIME': ''
    }
