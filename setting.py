import pymongo
import redis

from flask_caching import Cache

cache = Cache(config={'CACHE_TYPE': 'redis'})

RD_CLIENT = redis.StrictRedis()
client = pymongo.MongoClient(connect=False)
DB = client['sreal']
DB_USER = DB['user']
DB_POST = DB['post']
