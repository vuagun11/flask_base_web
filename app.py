from flask import Flask

from common.config import WEB_CONFIG
from views.user import UserView
from common.authentication import login_required
from setting import cache

app = Flask(__name__)
cache.init_app(app)
UserView.register(app, route_base='/user')

app.config['TEMPLATES_AUTO_RELOAD'] = False
app.config["CACHE_DEFAULT_TIMEOUT"] = 300
app.config.update(WEB_CONFIG)


@app.route('/')
def index():
    return 'Hello World!'


@login_required
def route_authen():
    return 'AUTHEN=True'


@app.route('/cached_router')
@cache.cached(timeout=30)
def cached_router():
    print('GET NEW DATA')
    return 'CACHED ROUTER'


if __name__ == '__main__':
    app.run()
