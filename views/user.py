from flask_classful import FlaskView, route
from flask import render_template, request, redirect, session
import setting


class UserView(FlaskView):
    @route('/login', methods=['POST', 'GET'])
    def index(self):
        if request.method == 'GET':
            return render_template('admin/user/login.html')
        elif request.method == 'POST':
            username = request.form.get("username")
            password = request.form.get("password")
            user = setting.DB_USER.find_one({"username": username, "password": password})
            if user:
                user['_id'] = str(user['_id'])
                session['user'] = {"username": user['username']}
                return redirect('/')
            return redirect('/user/login')

    def signout(self):
        del session['user']
        return redirect('/user/login')
